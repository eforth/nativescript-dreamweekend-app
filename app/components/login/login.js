var frameModule = require("ui/frame");

function signUp() {
	frameModule.topmost().navigate("components/register/register");
}

function signIn() {
	frameModule.topmost().navigate("login");
}

exports.signUp = signUp;
exports.signIn = signIn;