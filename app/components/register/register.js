var frameModule = require("ui/frame");
var Observable = require("data/observable").Observable;
var dialogModule = require("ui/dialogs");
var API = require("../../api");

var viewModel = new Observable();

viewModel.set("name", "");
viewModel.set("email", "");
viewModel.set("password", "");

function onLoad(args) {
	var page = args.object;
    page.bindingContext = viewModel;
}

function onNavBtnTap() {
	frameModule.topmost().navigate("components/login/login");
}

function resetViewModel() {
	viewModel.set("name", "");
	viewModel.set("email", "");
	viewModel.set("password", "");
}

function onSignUp() {

	var registerObject = {
		"name": viewModel.get("name"),
		"email": viewModel.get("email"),
		"password": viewModel.get("password")
	};

	fetch(API.PATHS.REGISTER_URL, {
	    method: "POST",
	    headers: API.HEADERS,
	    body: JSON.stringify(registerObject)
	}).then(function (response) {
		console.log("response", JSON.stringify(response)); 
		return response.json(); 
	}).then(function (r) {
	    console.log("Success", r);

	    var options = {
		    title: "User registration",
		    message: "User registered successfully",
		    okButtonText: "OK"
		};

		dialogModule.alert(options).then(function () {
			resetViewModel();
		});

	}).catch(function (e) {
        console.log("Error", e);

        var options = {
		    title: "User registration",
		    message: "An error has occurred",
		    okButtonText: "OK"
		};
		
		dialogModule.alert(options).then(function () {});
    });
}

exports.onSignUp = onSignUp;
exports.onNavBtnTap = onNavBtnTap;
exports.onLoad = onLoad;