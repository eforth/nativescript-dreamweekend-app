var appSettings = require("application-settings");
var APPLICATION_ID = '54BC6CC1-D405-792D-FFC8-64B3E5A21000';
var SECRET_KEY = '05E8EFF5-7F10-BCE9-FFC7-3AD68A5BCC00';
var VERSION = 'v1';
var API_URL = 'https://api.backendless.com';
var HEADERS = {
	"application-id": APPLICATION_ID,
	"secret-key": SECRET_KEY,
	"Content-Type": "application/json",
	"application-type": "REST"
};
var PATHS = {
	REGISTER_URL: API_URL + "/" + VERSION + "/users/register",
	AUTH_USER_URL: API_URL + "/" + VERSION + "/users/login"
};

module.exports = {
	API_URL:API_URL,
	HEADERS:HEADERS,
	PATHS:PATHS
};
